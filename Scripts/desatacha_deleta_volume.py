#!/usr/bin/env python3.9

import boto3

session = boto3.session.Session(profile_name="default")
ec2_res = session.resource(service_name="ec2", region_name="us-east-1")
ec2_cli = session.client(service_name="ec2", region_name="us-east-1")
ec2 = boto3.client('ec2')
sts_client = session.client("sts")
ownaccountid = sts_client.get_caller_identity().get("Account")

instancia_bacula = "i-xxxxxxxxxxxx"

Filtros = [
    {
        'Name': 'tag:Name',
        'Values': ["snapshot_cliente1"]
    },
    {
        'Name': 'tag:Name',
        'Values': ["snapshot_cliente2"]
    },

    {
        'Name': 'tag:Name',
        'Values': ["snapshot_cliente3"]
    }
]

volumes_atachados = []
instance = ec2_res.Instance(instancia_bacula)
volumes = instance.volumes.all()
for filtro in Filtros:
    for volume in volumes.filter(Filters=[filtro]):
        print("Desatachando Volume: ",volume.id)
        volumes_atachados.append(volume.id)
        volume.detach_from_instance(
            VolumeId=volume.id,
        )
        # Waiting for volume to become available
        waiter = ec2_cli.get_waiter('volume_available')
        waiter.wait(
            VolumeIds=[
                volume.id,
            ]
        )



for volume in volumes_atachados:
    volume_a_deletar = ec2_res.Volume(volume)
    volume_id = volume_a_deletar.id
    if volume_a_deletar.state == "available":
        volume_a_deletar.delete()
        print(f'Volume {volume_id} deletado com sucesso!')
    else:
        print(f"Nao foi possivel deletar o Volume {volume_id} attachado na nessa Instacia EC2")


