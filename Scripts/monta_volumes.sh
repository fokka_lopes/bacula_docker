#!/bin/bash 
# monta_volumes.sh
# Flavio R. Lopes - Flavio@links.inf.br

## Ativa o VirtualEnv
cd /root/boto3
. venv/bin/activate

## Executa o Script para montar os volumes

/root/Scripts/volumes.py
