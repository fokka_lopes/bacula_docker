#!/bin/bash 
# monta_volumes.sh
# Flavio R. Lopes - Flavio@links.inf.br

## Ativa o VirtualEnv
cd /root/boto3
. venv/bin/activate
python3.9 -m pip install boto3

## Executa o Script para destachar e deletar os volumes

/root/Scripts/desatacha_deleta_volume.py
#/usr/bin/sudo -n /usr/sbin/shutdown -P now
/usr/local/bin/aws ec2 stop-instances --instance-ids i-xxxxxxxxxxxx

